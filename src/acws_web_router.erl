%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. Apr 2018 2:00 PM
%%%-------------------------------------------------------------------
-module(acws_web_router).
-author("adean").

%% API
-export([init/2]).

-behaviour(cowboy_rest).

-define(APP, acws).
-define(JSON_HEADER,  #{"Content-Type" => "application/json"}).
-define(XML_HEADER,   #{"Content-Type" => "application/xml"}).
-define(TEXT_HEADER,  #{"Content-Type" => "text/text"}).
-define(OPTIONS_HEADER, #{"Access-Control-Allow-Origin" => "*",
                         "Access-Control-Allow-Headers" => "Content-Type,Access-Control-Allow-Headers, Authorization, X-Requested-With",
                         "Allow" => "HEAD,GET,PUT,DELETE,OPTIONS"}).


content_hdr(Ctype) -> get_hdr(#{"Content-Type" => Ctype}).

get_hdr(ContentHdr) -> maps:merge(ContentHdr, ?OPTIONS_HEADER).

init(Req, State = #{auth_mods := AuthMods, routes := Routes}) ->

  Path = cowboy_req:path(Req),
  Method = get_method_atom(Req),
  lager:debug("Going to Handle ~p", [Path]),

  Headers = get_headers(Req),

  QsValMap = add_body(Req, maps:from_list(cowboy_req:parse_qs(Req))),
  lager:debug("~p ~p ~p ~p", [Path, Method, Headers, QsValMap]),
  Req2 =
    try match_route(Routes, Method, Path) of
      {options, _, _, _} ->
        cowboy_req:reply(200, ?OPTIONS_HEADER, "", Req);
      {not_found, _, _} ->
        render_notfound(Req, Headers, Method, Path, QsValMap);
      {Method, Module, Func, Args, Opts} ->
        handle_route(Req, Method, Module, Func, Args, Opts, QsValMap, Headers, Path, AuthMods)
    catch
      Exception:Reason:StackTrace ->
        lager:error("match_route: ~p - ~p", [Exception, Reason]),
        lager:error("~p", [StackTrace]),
        render_server_error(Req, Headers)
    end,
  {ok, Req2, State};

init(Req, State) ->
  lager:warning("Don't know how to handle ~p, ~p", [Req, State]),
  {ok, Req, State}.

handle_route(Req, Method, Module, Func, Args, Opts, QsValMap, Headers, Path, AuthMods) ->
  try call_controller_module(Module, Func, [Method, Args, QsValMap, Headers], Opts, AuthMods, Req, Headers) of
    {text, Body} when is_binary(Body) ->
      cowboy_req:reply(200, get_hdr(?TEXT_HEADER), Body, Req);
    {text, Body} when is_list(Body) ->
      cowboy_req:reply(200, get_hdr(?TEXT_HEADER), binary:list_to_bin(Body), Req);
    {json, Body} when is_binary(Body) ->
      cowboy_req:reply(200, get_hdr(?JSON_HEADER), Body, Req);
    {json, Body} ->
      case acws_utils:map_to_json(Body) of
        {ok, BinaryBody} -> cowboy_req:reply(200, get_hdr(?JSON_HEADER), BinaryBody, Req);
        {error, _} ->
          Error = erlang:list_to_binary([<<"{\"error\": {\"code\": \"">>, "500", <<"\", \"short_message\": \"">>,
            "Server Error", <<"\", \"message\": \"">>, "Internal Server Error!", <<"\"}">>]),
          cowboy_req:reply(500, get_hdr(?JSON_HEADER), Error, Req)
      end;

    {xml, Body} ->
      cowboy_req:reply(200, get_hdr(?XML_HEADER), Body, Req);
    {binary, Hdr, Data} ->
      cowboy_req:reply(200, content_hdr(Hdr), Data, Req);
    {moved, Url} ->
      cowboy_req:reply(301, maps:put(<<"Location">>, Url, ?JSON_HEADER), "", Req);
    {redirect, Url} ->
      cowboy_req:reply(302, maps:put(<<"Location">>, Url, ?JSON_HEADER), "", Req);
    {gone, _} ->
      cowboy_req:reply(410, ?JSON_HEADER, Req);
    {error, 401} ->
      render_error(Req, Headers, 401, "Restricted", "This resource needs an authenticated session!");
    {error, 403} ->
      render_error(Req, Headers, 403, "Unauthorized", "You are not authorized for this resource!");
    {error, code} ->
      render_error(Req, Headers, code, "Error #{code}", "Server returned #{code}, contact admin!");
    {StatusCode, ContentHeader, Body} when is_atom(ContentHeader) ->
      cowboy_req:reply(StatusCode, get_hdr(get_content_header(ContentHeader)), Body, Req);
    _ ->
      render_notfound(Req, Headers, Method, Path, QsValMap)

  catch
    error:undef ->
      lager:error("Missing ~p:~p to match [Method, UrlFrags, Params, Headers]", [Module, Func]),
      render_server_error(Req, Headers);
    Exception:Reason:StackTrace ->
      lager:error("match_route: ~p - ~p", [Exception, Reason]),
      lager:error("~p", [StackTrace]),
      render_server_error(Req, Headers)
  end.

call_controller_module(M, F, A, Auths, AuthMods, Req, Headers) ->
    lager:debug("Going to call ~p:~p ~p", [M, F, A]),
    case auth_match(Auths, AuthMods, Req, Headers) of
      {true, _} -> apply(M, F, A);
      {false, Code} -> {error, Code};
      error -> lager:error("#{error} is not a valid return from pre_route controller. Has to be {true or false, http_code or nil}"),
               {error, 500}
    end.

auth_match([], _, _, _) -> {true, []};
auth_match(Auths, AuthMods, Req, Headers) ->
  try lists:foldl(
      fun(AuthName, Acc) ->
        lager:debug("auth name --> #{auth_name} #{inspect auth_mods}"),
        case maps:get(AuthName, AuthMods, []) of
          [] -> lager:error("Missing Route Check module #{auth_name} - Routes don't work as intended"),
                 Acc;
          {M, F} -> lager:debug("Calling pre route mod #{m} #{f}"),
                    case apply(M, F, [Req, Headers]) of
                      {false, Code} -> throw({false, Code});
                      {true, _} -> Acc
                    end
        end
      end, {true, 500}, Auths) of
    Result -> Result
  catch
    Result -> Result
  end.

get_headers(Req) ->
  Headers = cowboy_req:headers(Req),
  {RemoteIp, RemotePort} = cowboy_req:peer(Req),
  IpHeader =
    case maps:get(<<"x-forwarded-for">>, Headers, []) of
      [] -> maps:merge(Headers, #{<<"remote-ip">> => RemoteIp, <<"remote-port">> => RemotePort});
      Ip  -> maps:merge(Headers, #{<<"remote-ip">> => convert_to_ip(Ip), <<"remote-port">> => RemotePort})
    end,
  process_auth_headers(IpHeader).

process_auth_headers(Headers = #{<<"authorization">> := Auth}) ->
    case Auth of
      <<"Basic ", Tk/binary>> ->
        case string:split(":", base64:decode(Tk)) of
          [User, Pw] -> maps:put(<<"basic-auth">>, {User, Pw}, Headers);
          _ -> Headers
        end;
        _ -> Headers
    end;

process_auth_headers(Headers) -> Headers.

convert_to_ip(IpStr) ->
    try
      Sl = string:split(IpStr, "."),
      Sl2 = lists:map(fun(L) -> erlang:list_to_integer(L) end, Sl),
      erlang:list_to_tuple(Sl2)
    of
      Answer -> Answer
    catch
     _ -> {0,0,0,0}
    end.


read_body(Req0, Acc) ->
    case cowboy_req:read_body(Req0) of
        {ok, Binary, Req} -> {ok, << Acc/binary, Binary/binary >>, Req};
        {more, Binary, Req} -> read_body(Req, << Acc/binary, Binary/binary >>)
    end.


-spec add_body(Req :: map(), QsVals :: map()) -> map().
add_body(Req = #{has_body := true}, QsVals) ->
    case cowboy_req:header(<<"content-type">>, Req) of
      <<"application/json", _/binary>> ->
        {ok, Body, _Req2} = read_body(Req, <<>>),
        case acws_utils:json_to_map(Body) of
          {error, []} ->
            lager:error("Invalid Json found in the body ~p, ignoring", [Body]),
            QsVals;
          {ok, Map} ->
            maps:merge(QsVals, Map)
        end;
      _ ->
        {ok, Body, _Req2} = read_body(Req, <<>>),
        maps:put(raw_body, Body, QsVals)
    end;


add_body(_Req = #{has_body := false}, QsVals) -> QsVals.

%% <<<<#########  Render Error Messages ############
render_error(Req, Headers, Code, Msg, LongMsg) ->
    {Header, Body} = get_mimed_error_body(Headers, Code, Msg, LongMsg),
    cowboy_req:reply(500, Header, binary:list_to_bin(Body), Req).

render_server_error(Req, Headers) ->
  render_error(Req, Headers, <<"500">>, <<"Server Error">>, <<"The server encountered an error processing your request.">>).


render_notfound(Req, Headers, Method, Path, QsVals) ->
    lager:error("Route Not Found - ~p, ~p, ~p", [Method, Path, QsVals]),
    {Header, Body} = get_mimed_error_body(Headers, <<"404">>, <<"Not Found">>,
                          <<"Request cannot be processed. Resource not available or you sent an invalid request.">>),
    cowboy_req:reply(404, Header, Body, Req).

get_mimed_error_body(Headers, Code, ShortMsg, Msg) ->
    Mimes = binary:split(maps:get(<<"accept">>, Headers, <<"*/*">>), <<",">>, [global]),
    RequestType = maps:get(<<"content-type">>, Headers, <<"*/*">>),
    Result = acws_utils:find_value(fun(M) -> T = get_mime_type(M), { T /= [], T} end, [RequestType, Mimes]),
    case Result of
      {ok, xml} ->
        {?XML_HEADER, [<<"<?xml version=\"1.0\" encoding=\"UTF-8\" ?><error>">>,
                          <<"<code>">>, Code, <<"</code>">>,
                          <<"<short_message>">>, ShortMsg, <<"</short_message><message>">>, Msg, <<"</message>">>,
                          <<"</error>">>]};
      {ok, json} ->
        {?JSON_HEADER, [<<"{\"error\": {\"code\": \"">>, Code, <<"\", \"short_message\": \"">>,
                          ShortMsg, <<"\", \"message\": \"">>, Msg, <<"\"}">>]};
      NoMatch ->
        lager:error("No Content Type Match for ~p", [NoMatch]),
        {?TEXT_HEADER, [<<"code: ">>, Code, <<"\n short message: ">>, ShortMsg, <<"\n message: ">>, Msg, <<" \n">>]}

    end.

match_route(_, options, _Path) ->
  {options, [], [], []};

match_route(Routes, HttpMethod, Path) ->
    case acws_utils:find_value(
      fun(RoutePart) ->
        route_part_match(HttpMethod, Path, RoutePart)
      end, Routes)
    of
      {not_found, []} -> {not_found, [], []};
      {ok, {_Method, Module, Func, Args, Opts}} -> {HttpMethod, Module, Func, Args, Opts}
    end.

route_part_match(HttpMethod, Path, RoutePart) ->
  Result =
    case RoutePart of
      {M, R, C, F, O} when M == HttpMethod orelse M == any -> {M, R, C, F, O};
      {M, R, C, F} when M  == HttpMethod orelse M == any -> {M, R, C, F, []};
      _ -> []
    end,
  path_regex_match(Result, Path).

path_regex_match([], _Path) -> {false, []};
path_regex_match({M, Regex, Ctrl, Func, Opt}, Path) ->
  {ok, R} = re:compile(Regex, [caseless]),
  case re:run(Path, R, [{capture, all, binary}]) of
    {match, Result} -> {true, {M, Ctrl, Func, Result, Opt}};
    nomatch -> {false, []}
  end.


get_method_atom(#{method := Method}) ->
  case Method of
    <<"GET">> -> get;
    <<"POST">> -> post;
    <<"PUT">> -> put;
    <<"DELETE">> -> delete;
    <<"HEAD">> -> head;
    <<"PATCH">> -> patch;
    <<"OPTIONS">> -> options;
    _ -> invalid
  end;
get_method_atom(_) -> invalid.

-spec get_content_header(json | xml | text) -> map().
get_content_header(Header) ->
   case Header of
      json -> ?JSON_HEADER;
      xml  -> ?XML_HEADER;
      text -> ?TEXT_HEADER
   end.


get_mime_type(MType) ->
  case MType of
    <<"text/html">> -> html;
    <<"application/json">> -> json;
    <<"application/xml">> -> xml;
    <<"text/plain">> -> text;
    <<"text/csv">> -> text;
    _ -> []
  end.
