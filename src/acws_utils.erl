%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Apr 2018 6:18 AM
%%%-------------------------------------------------------------------
-module(acws_utils).
-author("adean").

-export([json_to_map/1, map_to_json/1, to_binary/1, find/2, find_value/2, convert_to_ip/1]).
-define(BQUOTE(V), [<<"\"">>, V, <<"\"">>]).

-spec json_to_map(binary()) -> {ok, map()} | {error, []}.
json_to_map(Bin) ->
  try jiffy:decode(Bin, [return_maps]) of
    Map -> {ok, Map}
  catch
    Exception:Reason ->
      lager:error("~p", [Bin]),
      lager:error("Json Conversion to map:  Error: ~p - ~p", [Exception, Reason]),
      {error, []}
  end.

-spec map_to_json(map()) -> {ok, binary()} | {error, []}.
map_to_json(Map) ->
  try jiffy:encode(Map) of
    Json -> {ok, Json}
  catch
    Exception:Reason ->
      lager:error("~p", [Map]),
      lager:error("Json Conversion from Map: Error: ~p - ~p", [Exception, Reason]),
      {error, []}
  end.

%%to_json(Map) when is_map(Map) ->
%%  binary:list_to_bin(
%%    [<<"{">>,
%%      to_json(lists:sort(fun(A, B) -> A > B end, maps:to_list(Map))),
%%      <<"}">>]
%%  );
%%
%%to_json([{K, V}]) -> [to_binary(K), <<":">>, to_json(V)];
%%to_json([{K, V} | Rest]) -> [to_json(Rest), <<",">> | [to_binary(K), <<":">>, to_json(V)]];
%%to_json(V = [C | _]) when not is_list(C) -> to_binary(V);
%%to_json(V) -> to_binary(V).

to_binary(V) when is_integer(V) -> erlang:integer_to_binary(V);
to_binary(V) when is_float(V) -> erlang:float_to_binary(V, [{decimals, 2}]);
to_binary(V = [C | _]) when is_list(V) andalso not is_list(C) andalso not is_tuple(C)->
  ?BQUOTE(binary:list_to_bin(V));   %make this is a string not another list.
to_binary(V) when is_binary(V) -> ?BQUOTE(V);
to_binary(V) when is_atom(V) -> ?BQUOTE(erlang:atom_to_binary(V, utf8)).

find(Func, List) ->
  try lists:foreach(
    fun(L) ->
      case Func(L) of
        false -> [];
        true -> throw({ok, L})
      end
    end, List) of
    _ -> {not_found, []}
  catch
    {ok, Result} -> {ok, Result}
  end.

find_value(Func, List) ->
  try lists:foreach(
    fun(L) ->
      case Func(L) of
        {false, _} -> [];
        {true, V} -> throw({ok, V});
        Msg -> lager:error("Bad Case clause ~p", [Msg])
      end
    end, List) of
    _ -> {not_found, []}
  catch
    Result -> Result
  end.

convert_to_ip(IpStr) ->
  try
    Sl = string:split(IpStr, "."),
    Sl2 = lists:map(fun(L) -> erlang:list_to_integer(L) end, Sl),
    erlang:list_to_tuple(Sl2)
  of
    Answer -> Answer
  catch
    _ -> {0,0,0,0}
  end.

