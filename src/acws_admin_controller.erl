%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Apr 2018 12:17 PM
%%%-------------------------------------------------------------------
-module(acws_admin_controller).
-author("adean").

%% API
-export([health_check/4]).

health_check(_, _UrlParts, _Args, _Headers) ->
  {json, #{code => 200, message => ok }}.
