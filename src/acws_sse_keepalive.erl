%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Aug 2018 11:47 AM
%%%-------------------------------------------------------------------
-module(acws_sse_keepalive).
-author("adean").

-behavior(gen_server).
%% API
-export([]).

-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

init(_) ->
  lager:info("Starting sse keep_alive pinger"),
  spawn(fun() -> keep_alive_ping() end),
  {ok, #state{}}.

handle_call(_Request, _From, State) ->
  Reply = ok,
  {reply, Reply, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


keep_alive_ping() ->
  receive
    _ ->  ok
  after 30000 ->
    gproc_ps:publish(l, ping, {})
  end,
  keep_alive_ping().