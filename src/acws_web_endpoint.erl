%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. Apr 2018 2:00 PM
%%%-------------------------------------------------------------------
-module(acws_web_endpoint).
-author("adean").

%% API
-export([start_link/0]).

-define(APP_NAME, acws).

start_link() ->
  RouteMap = get_routes(),
  Port = maps:get(web_port, RouteMap, 4000),
  Dispatch = cowboy_router:compile([
    {'_',
        get_sse_routes(RouteMap) ++
        [
          {"/[...]", acws_web_router, get_init_state(RouteMap)}
        ]
    }
  ]),
  lager:info("Starting Http Listener..."),
  {ok, _} = cowboy:start_clear(http_listener,
    [{port, Port}],
    #{env => #{dispatch => Dispatch}, idle_timeout => 86400000}
  ),
  lager:info("Listening on port ~p", [Port]),
  self().


get_init_state(RouteMap) ->
  AuthMods =
    lists:map(fun({Name, Module, Func}) -> {Name, {Module, Func}} end,
        maps:get(auth_modules, RouteMap, [])),
  #{auth_mods => AuthMods, routes => maps:get(routes, RouteMap, [])}.

get_sse_routes(RouteMap) ->
  SseRoutes = maps:get(sse_routes, RouteMap, []),
  lists:map(
    fun({Path, Module, _}) ->
      {Path, acws_sse, #{module => Module, id => 0}}
    end, SseRoutes).

get_routes() ->
  RouteConfig =
    case application:get_env(?APP_NAME, route_file, []) of
      {Dir, FileName} ->
        load_route_file(Dir, FileName);
      [] ->
        load_route_file(["priv"], "route.config")
    end,
  maps:from_list(RouteConfig).


-spec load_route_file(Dir :: list(), FileName :: string()) -> term().
load_route_file(Dir, FileName) ->
  case file:path_script([Dir], FileName) of
    {ok, Term, _} ->
      Term;
    Error ->
      lager:error("Route File ~p/~p not found or inaccessible. Error: ~p", [Dir, FileName, Error]),
      []
  end.
