%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Apr 2018 9:48 AM
%%%-------------------------------------------------------------------
-module(acws_default_controller).
-author("adean").

%% API
-export([]).

-export([home/4]).

-type http_method() :: 'get' | 'post' | 'put' | 'delete' | 'options'.

-spec home(http_method(), UrlParts :: list(), Params :: map(), Headers :: map())
      -> {any(), list() | binary()}.
home(get, UrlParts, Params, Headers) ->
  lager:debug("default_controller:home - ~p ~p ~p", [UrlParts, Params, Headers]),
  {text, <<"returned from home method in default controller">>}.
