%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 19. Sep 2019 10:53 AM
%%%-------------------------------------------------------------------
-module(acws_sse_sample).
-author("adean").

%% API
-behavior(acws_sse).

-export([init/0, validate_token/2, process_init_data/2, process_data/2]).

init() ->
  lager:debug("Initialized ~p", [?MODULE]).

validate_token(Req, AuthToken) ->
  {ok, #{}}.

%%get_filter_spec(Req, State) ->
%%  erlang:error(not_implemented).
%%
%%get_init_state(Req, State) ->
%%  erlang:error(not_implemented).

process_init_data(_Req, State) ->
  spawn(fun() -> sample_stream() end),
  {[{1, "sample.init.data", <<"{\"sample\": 123}">>}], State#{id := 1}}.

process_data(Req,  {{K1, K2, K3, Payload}, State = #{id := Id}}) ->
  {list_to_binary([K1, ".", K2, ".", K3]), Payload, State#{id := Id + 1}}.

sample_stream() ->
  receive
    _ -> ok
  after 10000 ->
    lager:debug("Publishing Event....."),
    DateTime = list_to_binary([<<"{\"sample\":">>, integer_to_binary(os:system_time(seconds)), <<"}">>]),
    gproc_ps:publish(l, chunk, {<<"sample">>,<<"proc">>,<<"data">>, DateTime})
  end,
  sample_stream().