%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Aug 2018 1:02 AM
%%%-------------------------------------------------------------------
-module(acws_sse).
-author("adean").

-behavior(cowboy_loop).
%% API
-export([init/2, info/3, build_filter_spec/1]).

-define(NIL, undefined).
-define(SSE_HEADERS, #{<<"content-type">> => <<"text/event-stream">>, <<"cache-control">> => <<"no-cache">>}).

-callback validate_token(Req, AuthToken :: binary())
      -> {error, binary()} | {ok, map()}
  when Req::cowboy_req:req().

-callback get_filter_spec(Req, {AuthResult :: map(), FilterSpec :: binary()})
      -> list()
  when Req::cowboy_req:req().

-callback get_init_state(Req, {AuthResult :: map(), State :: map()}) -> map()
  when Req::cowboy_req:req().

-callback process_init_data(Req, InitState :: map()) -> _
  when Req::cowboy_req:req().

-callback process_data(Req, {Event :: {binary(), binary(), binary(), binary()}, State :: map()}) -> {map(), binary(), binary()}
  when Req::cowboy_req:req().


init(Req, State = #{module := Module}) ->
%%  lager:debug("Going to Handle ~p", [State]),
  Module:init(),
  Qs = cowboy_req:parse_qs(Req),
  AuthToken =
    case cowboy_req:binding(token, Req, ?NIL) of
      {?NIL, _} ->
        case get_headers(Req) of
          #{<<"basic-auth">> :=  {<<"token">>, Token}} -> Token;
          _ -> ?NIL
        end;
      {Token, _} -> Token;
      _ -> ?NIL
    end,

%%  lager:debug("Initialized -> #{inspect filter}, #{inspect token}"),
  Result =
    case call(Req, Module, validate_token, AuthToken) of
      {error, Msg} ->
        lager:error("acws_sse:validate_token - ~p", [Msg]),
        Req2 = cowboy_req:reply(401, #{}, <<"Invalid or Expired Token \n">>, Req),
        {stop, Req2, undefined_state};

      {ok, AuthResult} ->
        setup_sse(Req, Module, AuthResult, maps:from_list(Qs), State)
    end,
  Result;

init(Req, State) ->
  lager:error("Invalid Module in SSE Routes Config ~p, ~p", [Req, State]),
  {ok, Req, State}.

info(eof, Req, State) ->
  lager:debug("eof received. Stopping the SSE Module "),
  {stop, Req, State};

info({gproc_ps_event, ping, _}, Req, State) ->
  cowboy_req:stream_body(<<":\n">>, nofin, Req),
  {ok, Req, State};

%% calling module of this behavior has to implement process_data and return the sse string (binary) that needs
%% to be transmitted.
info({gproc_ps_event, chunk, Data = {_K1, _K2, _K3, _Msg}}, Req, State = #{module := Module, id := Id}) ->
  {Event, Payload, UpdState} =
    case call(Req, Module, process_data, {Data, State}) of
      {Event1, Payload1, UpdState1 = #{id := UpdId}} when UpdId == Id -> {Event1, Payload1, UpdState1#{id := Id + 1}};
      {Event1, Payload1, UpdState1} -> {Event1, Payload1, UpdState1}
    end,
  EventData = sse_event(maps:get(id, UpdState), Event, Payload, 30),
  cowboy_req:stream_body(EventData, nofin, Req),
  {ok, Req, UpdState};

%% will stop the loop
info(Msg, Req, State) ->
  lager:warning("Caught an unhandled msg:   ~p, ~p", [Msg, State]),
  {ok, Req, State}.

setup_sse(Req, Module, AuthResult, Qs, State) ->
%%  lager:debug("Auth Result - ~p", [AuthResult]),
  gproc_ps:subscribe(l, ping),
  UpdatedState =
    case call(Req, Module, get_init_state, {AuthResult, State}) of
      no_call -> State;
      NewState -> NewState
    end,
%%  lager:debug("What we get from get_init_state ~p", [UpdatedState]),
  Req2 = cowboy_req:stream_reply(200, ?SSE_HEADERS, Req),
  UpdatedState2 =
    case call(Req, Module, process_init_data, UpdatedState) of
      no_call -> UpdatedState;
      {Result, ModifiedState} ->
        send_init_data(Req2, Result),
        ModifiedState;
      _ ->
        lager:error("Invalid return from process_init_data.  The return should be {list(), State | Modified State}")
    end,
  case maps:get(filter, Qs, ?NIL) of
    ?NIL ->
      lager:debug("Subscribing to catch all filterspec"),
      gproc_ps:subscribe(l, chunk);
    Filter ->
      case call(Req, Module, get_filter_spec, {AuthResult, Filter}) of
        no_call ->
          gproc_ps:subscribe(l, chunk);
        FilterSpec ->
          lager:debug("subscribe-event: ~p", [FilterSpec]),
          gproc_ps:subscribe_cond(l, chunk, FilterSpec)
      end
  end,
  {cowboy_loop, Req2, UpdatedState2}.

get_headers(Req) ->
  Headers = cowboy_req:headers(Req),
  {RemoteIp, RemotePort} = cowboy_req:peer(Req),
  IpHeader =
    case maps:get(<<"x-forwarded-for">>, Headers, []) of
      [] -> maps:merge(Headers, #{<<"remote-ip">> => RemoteIp, <<"remote-port">> => RemotePort});
      Ip  -> maps:merge(Headers, #{<<"remote-ip">> => acws_utils:convert_to_ip(Ip), <<"remote-port">> => RemotePort})
    end,
  process_auth_headers(IpHeader).

-spec process_auth_headers(Headers :: #{binary() => binary()}) -> map().
process_auth_headers(Headers = #{<<"authorization">> := Auth}) ->
  case Auth of
    <<"Basic ", Tk/binary>> ->
      case string:split(":", base64:decode(Tk)) of
        [User, Pw] -> maps:put(<<"basic-auth">>, {User, Pw}, Headers);
        _ -> Headers
      end;
    <<"Digest ", _Tk/binary>> ->
      maps:put(<<"diget-auth">>, {not_implented, not_implemented}, Headers);
    _ -> Headers
  end;

process_auth_headers(Headers) -> Headers.

send_init_data(Req, Data) when is_list(Data) ->
  lists:foldl(fun({Id, Event, Payload}, Req0) ->
    cowboy_req:stream_body(sse_event(Id, Event, Payload, 30), nofin, Req0)
    end, Req, Data).

sse_event(Id, Event, Payload, Retry) ->
  list_to_binary(["retry:", integer_to_binary(Retry), "\nid:", integer_to_binary(Id), "\nevent:", Event, "\ndata:", Payload, "\n\n"]).

-spec call(Req::cowboy_req:req(), Handler :: module(), CallBack :: atom(), Arg :: any()) -> _.
call(Req, Handler, Callback, Arg) ->
%%  lager:debug("Checking whether the function is exported ~p:~p - ~p", [Handler, Callback, erlang:function_exported(Handler, Callback, 2)]),
  case erlang:function_exported(Handler, Callback, 2) of
    true ->
      try
        Handler:Callback(Req, Arg)
      catch Class:Reason:StackTrace ->
        erlang:raise(Class, Reason, StackTrace)
      end;
    false ->
      no_call
  end.

% P1 = [<<"chan">>,<<"vm">>,<<"queue">>], P2 = <<"baaaaac">>
build_filter_spec({P1, ?NIL, P3}) when is_list(P1) ->
  Filters = [ {'==', '$1', Part} ||  Part <- P1],
  SpecParts = list_to_tuple(['or', {'==', '$3', P3} | Filters]),
  [{{'$1', '$2', '$3', '$4'}, [SpecParts], [true]}];

% P1 = <<"chan">>, P2 = <<"baaaaac">>
build_filter_spec({P1, ?NIL, P3}) ->
  SpecParts = {'and', {'==', '$3', P3}, {'==', '$1', P1}},
  [{{'$1', '$2', '$3', '$4'}, [SpecParts], [true]}].
