%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. Apr 2018 2:29 PM
%%%-------------------------------------------------------------------
-module(acws_pre_route_controller).
-author("adean").

%% API
-export([check_logged_in/2]).

-spec check_logged_in(Req :: map(), Headers :: list()) -> {true , _} | {false | binary()}.
check_logged_in(Req, _Headers) ->
  Path = cowboy_req:path(Req),
  lager:debug("Authent check for ~p", [Path]),
  {true, []}.



