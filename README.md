acws
=====

A wrapper library around cowboy to simplify development of REST services.  

It does not cover all aspect of REST and content types. However it supports regular HTTP requests as well ability setup SSE streams.

Routes
------
Define your routes in priv folder:

```erlang
[
  {web_port, 4000},
    {auth_modules, [
      {logged_in, acweb_pre_route_controller, check_logged_in}
    ]},
    {routes, [
      {get, <<"^/local/health-check">>, acws_admin_controller, health_check},
      {get, "^/$", default_controller, home},

      %% admin routes %%
      {any, <<"^/admin/login$">>, acweb_admin_controller, login}
      {:get, "^/admin/dashboard$", Controllers.AdminController, :dashboard, [:logged_in]},
    ]},
    {sse_routes, [
      {"/sse/some_events", sse_some_events, []}
    ]}
].
```
Auth Guards
-----------
You can define **auth guards** using `auth_modules` above, that return {true, []} | {false, <<"some message passed with 403">>}. 
Then these **auth guards** can be used to "guard" routes.  For eg:

```
    {auth_modules, [
      {logged_in, acweb_pre_route_controller, check_logged_in}
    ]},
```
guards the route:

`{:get, "^/admin/dashboard$", Controllers.AdminController, :dashboard, [:logged_in]},`

The library will parse any Basic-Auth headers and will pass them to the auth module in `headers` map as <<"basic-auth">> := {<<"user">>, <<"password">>}.
Digest authentication and the challenge response will be handled by the library in a subsequent release.

SSE Routes
----------
Implement `aws_sse` *behavior* in the sse module.

This handler will get activated for each SSE request and the following description applies to the lifecycle of that request.

Most of the base SSE feature is provided by this library. However, your module would require to implement following methods to supply 
the required data for the SSE routes to be active.

```erlang
-callback validate_token(Req, AuthToken :: binary())
      -> {error, binary()} | {ok, map()}
  when Req::cowboy_req:req().

-callback get_filter_spec(Req, {AuthResult :: map(), FilterSpec :: binary()})
      -> list()
  when Req::cowboy_req:req().

-callback get_init_state(Req, {AuthResult :: map(), State :: map()}) -> _
  when Req::cowboy_req:req().

-callback req_init_data(Req, InitState :: map()) -> _
  when Req::cowboy_req:req().
```

callback `validate_token`: you return either `{error, <<"Some message">>}` or `{ok, map()}` the map can contain any data fields that 
acquired using the auth token, then that can be used subsequently.  You can also return `ok` and an empty map to bypass authorization. 
Currently, this library only supports http basic-auth and assume you are using a SSL connection.  
Digest authentication will be implemented in subsequent releases.

callback `get_init_state`: This is initial state of the SSE you want to setup. The state of this event handler will be passed in to this 
function and you must return the state unmodified or by adding additional data that would be required by future state of this handler.

callback `req_init_data`: This where you will call a function in your application to generate initial data for the connection.  
May be some historical data already collected.

callback `get_filter_spec`: This library uses `gproc_ps` internally and this function expects a `gproc_ps` filter that would be registered
to receive data updates from an upstream service. You would requie to use `gproc_ps` publish to publish data that would be received by this
SSE handler.  For eg:

_SSE Keep Alive handler_ publishes *pings* using `gproc_ps:publish(l, ping, {})`, this handler will subscribe to these pings using `gproc_ps:subscribe(l, ping)`

If you return ?NIL (undefined) from this function, this handler will consume anything published via `gproc_ps:publish(l, topic, data)` and will send
these data to SSE consumers.

Build
-----

    $ rebar3 compile
